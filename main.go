package progressbar

import (
	"fmt"
	"strings"
	"time"

	"golang.org/x/term"
)

// Couln't get terminal size from env "COLUMNS" so have to use external package
var termDim, _, _ = term.GetSize(0)
var emptyChar = "░"
var fullChar = "█"

func WithDuration(fullText string, duration time.Duration) {
	interval := duration / time.Duration(termDim)

	fmt.Println(fullText, duration)

	for step := 0; step < termDim; step++ {
		bar := make([]string, termDim)
		for empty := range bar {
			bar[empty] = emptyChar
		}

		for full := 0; full < step+1; full++ {
			bar[full] = fullChar
		}

		displayBar := strings.Join(bar, "")
		fmt.Printf("%v\x0c", displayBar)
		time.Sleep(interval)
	}
	fmt.Println("")
}

func WithSteps(step int, total int) {
	interval := float64(termDim) / float64(total)

	bar := make([]string, termDim)
	for empty := range bar {
		bar[empty] = emptyChar
	}

	for full := 0; full < (step+1)*int(interval); full++ {
		bar[full] = fullChar
	}

	displayBar := strings.Join(bar, "")
	fmt.Printf("%v\x0c", displayBar)

	if step >= total-1 {
		for complete := range bar {
			bar[complete] = fullChar
		}
		displayBar := strings.Join(bar, "")
		fmt.Printf("%v\x0c", displayBar)
		fmt.Println("")
	}
}
