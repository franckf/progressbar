## Progress Bar

![pipeline status](https://gitlab.com/franckf/progressbar/badges/main/pipeline.svg)

![coverage report](https://gitlab.com/franckf/progressbar/badges/main/coverage.svg)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/franckf/progressbar)](https://goreportcard.com/report/gitlab.com/franckf/progressbar)

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/franckf/progressbar.svg)](https://pkg.go.dev/gitlab.com/franckf/progressbar)

Simple and quick package to display progress bars in terminal.

Now with durations but more to come.

![](capture.png)

### Install

`go get gitlab.com/franckf/progressbar`

### Example

```
package main

import (
	"fmt"
	"time"

	"gitlab.com/franckf/progressbar"
)

func main() {
	start := time.Now()
	tensec := time.Second * 10
	progressbar.WithDuration("stand-by text", tensec)
	elapsed := time.Since(start)
	fmt.Println(elapsed)

	alphabet := []string{"a", "b", "c", "d", "e"}
	fmt.Println("alphabet")
	for i := range alphabet {
		// dealing with every step
		time.Sleep(time.Second)
		progressbar.WithSteps(i, len(alphabet))
	}
}
```
